import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { RestApiService } from '../rest-api.service';

@Component({
  // selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  title = 'Login Here';
  email = '';
  password = '';

  btnDisabled = false;

  constructor(
    private router: Router,
    private data: DataService,
    private rest: RestApiService 
  ) { }

  validate() {
    if ( ! this.email) {
      this.data.error('email is required');
      return false;
    }

    if ( ! this.password) {
      this.data.error('password is required');
      return false;
    }

    return true;
  }

  ngOnInit() {
  }

  async login() {
    // var self = this;

    try {
      if (this.validate()) {
        const data = await this.rest.post(
          `http://localhost:3030/api/account/signin`,
          {
            email: this.email,
            password: this.password
          }
        );

        if (data['success']) {
          localStorage.setItem('token', data['token']);
          // ???? dont get it
          this.router.navigate(['/']);
          this.data.success(data['msg']);
          await this.data.getProfile();
          console.log(data['msg']);
        } else {
          this.data.error(data['msg']);
          console.log(data['msg']);
        }
      }
    } catch(error) {
      this.data.error(error['message']);
    }
  }
}
