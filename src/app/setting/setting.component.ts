import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { RestApiService } from '../rest-api.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  btnDisabled = false;
  currentSettings: any;

  constructor(private data: DataService, private rest: RestApiService) { }

  async ngOnInit() {
    try {
      if ( ! this.data.user) {
        await this.data.getProfile();
        console.log(`get profile: ${JSON.stringify(this.data.user)}`);
      }
      this.currentSettings = Object.assign({
        newPwd: '',
        pwdConfirm: ''

      }, this.data.user);
    } catch(error) {
      this.data.error(error);
    }
  }

  validate(settings) {
    if ( ! settings['name']) {
      this.data.error('Please enter name');
    }
    
    if ( ! settings['email']) {
      this.data.error('Please enter email');
    }

    if ( ! settings['newPwd']) {      
      if ( ! settings['pwdConfirm']) {
        this.data.error('Please enter confirmation password');
      }

      if (settings['newPwd'] !== settings['pwdConfirm']) {
        this.data.error('Password are mot matched');
      }
    }


    return true;
  }

  // ??? when does it(func update) called???
  async update() {

    this.btnDisabled = true;
    try {
      if (this.validate(this.currentSettings)) {
        const data = await this.rest.post(
          `http://localhost:3030/api/account/profile`,
          {
            name: this.currentSettings['name'],
            email: this.currentSettings['email'],
            password: this.currentSettings['newPwd'],
            isSeller: this.currentSettings['isSeller']
          }
        );
        
        data['success'] ? 
        (this.data.getProfile(), this.data.success(data['msg'])) :
        this.data.error(data['msg']);
      }


    } catch(error) {
      this.data.error(error['message']);
    }
    
    this.btnDisabled = false;
  }

}
