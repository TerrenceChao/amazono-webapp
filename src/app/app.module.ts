import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';


import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// https://www.npmjs.com/package/@ng-bootstrap/ng-bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './/app-routing.module';


import { AuthGuardService } from './auth-guard.service';
import { DataService } from './data.service';
import { RestApiService } from './rest-api.service';
import { ProfileComponent } from './profile/profile.component';
import { AddressComponent } from './address/address.component';
import { SettingComponent } from './setting/setting.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegistrationComponent,
    LoginComponent,
    ProfileComponent,
    AddressComponent,
    SettingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    AuthGuardService,
    DataService,
    RestApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
