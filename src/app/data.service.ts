import { Injectable } from '@angular/core';

// ???  NavigationStart ??
import { NavigationStart, Router } from '@angular/router';

import { RestApiService } from './rest-api.service';

@Injectable()
export class DataService {

  message = ''
  messageType = 'danger';

  user: any;

  constructor(private router: Router, 
              private rest: RestApiService) { 
    // ???
    this.router.events.subscribe(event => {
      if(event instanceof NavigationStart) {
        this.message = '';
      }
    });
  }

  error(message) {
    this.messageType = 'danger';
    this.message = message;
  }

  success(message) {
    this.messageType = 'success';
    this.message = message;
  }

  warning(message) {
    this.messageType = 'warning';
    this.message = message;
  }

  async getProfile() {

    console.log(`data getProfile :::`);
    try {
      console.log(`local storage: ${JSON.stringify(localStorage)}`);
      if (localStorage.getItem('token')) 
      {
        const data = await this.rest.get(`http://localhost:3030/api/account/profile`);
        this.user = data['user'];
        console.log(`data getProfile => ${JSON.stringify(this.user)}`);
      }
    } catch(error) {
      this.error(error);
      console.log(`data getProfile ERROR => ${JSON.stringify(error)}`);
    }

  }
}
