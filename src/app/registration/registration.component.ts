import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { RestApiService } from '../rest-api.service';
import { DataService } from '../data.service';

@Component({
  selector: 'registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  title = 'Register Here';
  name = '';
  email = '';
  password = '';
  password1 = ''
  isSeller = false;

  btnDisabled = false;

  constructor(
    private router: Router,
    private data: DataService, // ?? 'private' make it working. why? 
    private rest: RestApiService
  ) { }

  ngOnInit() {
  }

  validate() {
    if ( ! this.name) {
      this.data.error('name is reqoured');
      return false;
    }

    if ( ! this.email) {
      this.data.error('email is required');
      return false;
    }

    if ( ! this.password) {
      this.data.error('password is required');
      return false;
    }

    if ( ! this.password1) {
      this.data.error('confirmed password is required');
      return false;
    }

    if (this.password != this.password1) {
      this.data.error('password do not match.');
      return false;
    }

    return true;
  }

  async register() {
    // var self = this;
    console.log(`test register`);

    this.btnDisabled = true;
    try {
      if (this.validate()) {
        const data = await this.rest.post(
          `http://localhost:3030/api/account/signup`,
          {
            email: this.email,
            name: this.name,
            password: this.password 
          }
        );

        if (data['success']) {
          localStorage.setItem('token', data['token']);
          this.data.success(`Registration successful !`);
          await this.data.getProfile();
          console.log(data['msg']);
        } else {
          this.data.error(data['msg']);
          console.log(data['msg']);
        }
      }

      this.btnDisabled = false;

    } catch(error) {
      this.data.error(error['message']);
    }
    
  }


}
