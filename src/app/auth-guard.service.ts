import { Injectable } from '@angular/core';

// ???
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }

  /**
   * ??? dont get it  !!!!
   * @param route 
   * @param state 
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(localStorage.getItem('token')) {
      // this.router.navigate(['/']);
      // return true;

      // 有 token 才能導向 '/profile'
      return state.url.startsWith('/profile') ? true : (this.router.navigate['/'], false);
    } else {
      // return false;
      
      // 沒有 token 就只能導向 '/''
      return state.url.startsWith('/profile') ? (this.router.navigate['/'], false) : true;
    }
  }
}
